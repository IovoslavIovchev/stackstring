/// An error occurs when there are more than `L` bytes are attempted to be
/// pushed into a `String<L>` or when a `String` is being created with less than `L` bytes.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Error {
    pub len: usize,
    pub tried: usize,
}

impl Error {
    pub fn new(len: usize, tried: usize) -> Self {
        Self { len, tried }
    }
}

impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Tried to push {} bytes into a string of size {}",
            self.tried, self.len
        )
    }
}

impl std::error::Error for Error {}
