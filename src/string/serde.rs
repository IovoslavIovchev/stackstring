use super::String;

use core::convert::TryFrom;
use serde::{de::Error, Deserialize, Deserializer, Serialize, Serializer};

impl<const L: usize> Serialize for String<L> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}

impl<'de, const L: usize> Deserialize<'de> for String<L> {
    fn deserialize<D>(deserializer: D) -> Result<String<L>, D::Error>
    where
        D: Deserializer<'de>,
    {
        // FIXME: try to make this a &str
        let s: std::string::String = Deserialize::deserialize(deserializer)?;

        Self::try_from(s.as_str())
            .map_err(|e| D::Error::custom(format!("Expected a String with len {}, got {}", L, e)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde::{Deserialize, Serialize};
    use serde_json::{from_str, to_string};

    #[derive(Serialize, Deserialize)]
    struct S {
        x: String<10>,
    }

    #[test]
    fn serialize() {
        let json_str = r#"{
            "x": "somestring"
        }"#;

        let s: S = from_str(json_str).unwrap();

        assert_eq!("somestring", s.x);
    }

    #[test]
    fn deserialize() {
        let x = String::try_from_str_padded("asd").unwrap();

        let s = S { x };

        let serialized = to_string(&s).unwrap();

        assert_eq!(r#"{"x":"asd       "}"#, serialized);
    }
}
