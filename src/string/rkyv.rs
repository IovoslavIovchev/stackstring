use super::String;

use rkyv::{Archive, Archived, Deserialize, Fallible, Serialize};

impl<const L: usize> Archive for String<L> {
    type Archived = String<L>;
    type Resolver = [<u8 as Archive>::Resolver; L];

    #[inline]
    unsafe fn resolve(&self, pos: usize, resolver: Self::Resolver, out: *mut Self::Archived) {
        // SAFETY: a stackstring is just an array of bytes
        let out = out.cast::<[u8; L]>();
        self.0.resolve(pos, resolver, out)
    }
}

impl<S: Fallible + ?Sized, const L: usize> Serialize<S> for String<L> {
    #[inline]
    fn serialize(&self, serializer: &mut S) -> Result<Self::Resolver, S::Error> {
        self.0.serialize(serializer)
    }
}

impl<D: Fallible + ?Sized, const L: usize> Deserialize<String<L>, D> for Archived<String<L>> {
    #[inline]
    fn deserialize(&self, deserializer: &mut D) -> Result<String<L>, D::Error> {
        unsafe {
            let mut res = core::mem::MaybeUninit::<[u8; L]>::uninit();
            let ptr = res.as_mut_ptr().cast::<u8>();

            for (i, x) in self.bytes().enumerate() {
                ptr.add(i).write(x.deserialize(deserializer)?);
            }

            Ok(String(res.assume_init()))
        }
    }
}
