use stackstring::String;

use core::convert::TryInto;
use rkyv::{
    archived_root,
    ser::{serializers::AlignedSerializer, Serializer},
    AlignedVec, Archive, Deserialize, Infallible, Serialize,
};

#[derive(Archive, Deserialize, Serialize, PartialEq, Debug)]
struct S {
    st: String<3>,
    x: u8,
}

fn main() {
    let value = S {
        st: "abf".try_into().unwrap(),
        x: 99,
    };

    let mut serializer = AlignedSerializer::new(AlignedVec::new());
    serializer
        .serialize_value(&value)
        .expect("failed serializing value");

    let buf = serializer.into_inner();
    assert_eq!(buf.len(), 4);

    let archived = unsafe { archived_root::<S>(buf.as_ref()) };

    assert_eq!(archived.st, value.st);

    let deserialized = archived
        .deserialize(&mut Infallible)
        .expect("failed deserializing value");

    assert_eq!(value, deserialized);
}
